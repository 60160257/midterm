/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.midterm;

import java.io.Serializable;

/**
 *
 * @author acer
 */
public class Product implements Serializable{

    private String productName;
    private String productBrand;
    private String price;

    @Override
    public String toString() {
        return "Name =  " + productName
                + "   ,Brand =  " + productBrand
                + "   ,price =  " + price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductBrand() {
        return productBrand;
    }

    public void setProductBrand(String productBrand) {
        this.productBrand = productBrand;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Product(String productName, String productBrand, String price) {
        this.productName = productName;
        this.productBrand = productBrand;
        this.price = price;

    }
    
}
