/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monk.midterm;

/**
 *
 * @author acer
 */
public class TestProductService {
    public static void main(String[] args) {
        System.out.println(ProductService.getProducts());
        ProductService.addProduct(new Product("Milk","ChaThai","30"));
        System.out.println(ProductService.getProducts());
        
        Product upProduct = new Product("Milkpink","ChaThai","30");
        ProductService.updateProduct(3, upProduct);
        System.out.println(ProductService.getProducts());
        
        ProductService.delProduct(upProduct);
        System.out.println(ProductService.getProducts());
        ProductService.delProduct(1);
        System.out.println(ProductService.getProducts());
        
    }
}
